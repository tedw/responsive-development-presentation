#Responsive Web Design Overview

This slide deck aims to provide an overview of RWD and associated terms.

##What is Responsive?
Coined by [Ethan Marcotte](http://ethanmarcotte.com/) in his 2010 article, [Responsive Web Design](http://alistapart.com/article/responsive-web-design), responsive web design (RWD) was originally defined as using fluid grids, flexible images, and media queries to deliver elegant visual experiences across all devices. Today, responsive has become an umbrella term for all aspects of creating websites optimized for devices of any size or capability.

##How to View
Clone the repo to your computer and open index.html in your browser.

You can also view it online here **[http://bit.ly/responsivedev](http://bit.ly/responsivedev)**

---

Built using **[reveal.js](http://lab.hakim.se/reveal-js/)**